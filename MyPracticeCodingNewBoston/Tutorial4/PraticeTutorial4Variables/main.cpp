/* 
 * File:   main.cpp
 * Author: ChaMarie Spears
 * Tutorial_4: Practice Variables 
 *
 * Created on February 27, 2014, 3:54 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {

    int tuna = 6;
    cout << tuna << endl;
    
    int a = 4;
    int b = 21;
    int sum = 48 - 7;
    
    cout << sum << endl;
    return 0;
}

