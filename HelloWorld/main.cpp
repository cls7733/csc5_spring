/* 
 * File:   main.cpp
 * Author: ChaMarie Spears
 *
 * Created on February 18, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream> // Adds i/o to my program
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Hello World" << endl; //Semicolon denotes end of the statement
    return 0;
}

