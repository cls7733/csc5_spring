/* 
 * File:   main.cpp
 * Name: ChaMarie
 * Student ID: 2427116
 * Date: 2/25/14
 * HW: 1
 * Problem: 1-7
 * I certify this is my own work and code
 *
 * Created on February 25, 2014, 2:05 PM
 */
//Problem 1 Enter the code in 1.8
#include  <iostream> 
 using namespace std;
  int main ()  {
     
 //Declare Variables
 int number_of_pods, peas_per_pod, total_peas;

 cout << "Hello \n"; //Problem 2 
 
 //Prompt user to enter a number
 cout << "Press return after entering a number.\n";
 
 //Prompt user to enter the number of pods
 cout << "Enter the number of pods:\n";
 
 //User inputs the number of pods
 cin >> number_of_pods;
 
 //Prompt user to enter number of peas in the pod
 cout << "Enter the number of peas in a pod:\n";
 
 //User input the peas per pod
 cin >> peas_per_pod;
 
 //Calculates the number of pods * peas in pod and gets the total of peas
 //total_peas = number_of_pods * peas_per_pod; 
 
 //Calculates the number of pods divide peas in pod and gets the total of peas
 //total_peas = number_of_pods / peas_per_pod; //Problem 3
 
 total_peas = number_of_pods + peas_per_pod; //Problem 4
 
 cout << "If you have ";
 //Displays the users inputs for pods
 cout << number_of_pods;
 
 cout << " pea pods\n";
 
 cout << "and ";
 
 //Displays the users inputs peas
 cout << peas_per_pod;
 
 cout << " peas in each pod, then\n";
 
 cout << "you have ";
 
 //Outputs total peas in pods
 cout << total_peas;
 
 cout << " peas in all the pods.\n";
 
 cout << "Good-bye\n"; //Problem 2
 
 //Beginning of Problem 5
 
 //Declares Variables
 int num1, num2, totalProduct, totalSum; 
 
 //Prompt user to enter 2 numbers
 cout << "Please enter a two numbers.\n";
 
 //User inputs for the number
 cin >> num1 >> num2;
 
 //Calculate num1 and num2
 totalProduct = num1 * num2;
 totalSum = num1 + num2;
 
 //outputs the product
 cout << "The product of the numbers are: \n";
 cout << totalProduct << endl;
 
 cout << "The sum of the numbers are: \n";
 cout << totalSum << endl;
 
 //Beginning Problem 6
 //extra space between the <: #include < iosteam> and omit a < in the iostream: #include iostream>
 //omit the int from int main (): main ()
 //Omit or misspell the word main: int ain ()
 //Omit one of the (), then omit both the ().
/*
 Continue in this fashion, deliberately misspelling identifiers (cout,
cin, and so on). Omit one or both of the << in the cout statement;
leave off the ending curly brace }.
 */
 
 //Error Messages
 //a. iostream space: fatal error: iostream: No such file or directory
 /* b. #include expects "Filename" or <FILENAME>
       cout was not declared in this scope
       cin was not declared in this scope
       endl was not declared in this scope
  */
  //c. the program still runs
  //d. no specific error but the program would not compile
 /*e. warining: extended initializer lists only available with -std=c++11 or -std=gnu++11 [enabled by default]
      expected primary-expression before int
      expected } before int
      peas_per_pod was not declared in this scope
      total_peas_per_pod was not declared in this scope
      cout does not name a type
      cin does not name a type
      total_peas does not name a type
      totalProduct does not name a type
      totalSum does not name a type
      total_peas does not name a type
      expected unqualified-id before return
      expected declaration before } token
      expected , ; before int
  */
  /*f. left off c in cout: out was not declared in this scope
       left off c in cin: in was not declared in this scope
       left off the << in cout: expected ; before string constant
       left off the } at the end of return: expected } at the end of input
   
   */
  
 //Beginning Problem 7
 cout << "***************************************************\n";
 cout <<     "C C C              S S S S                !!\n";
 cout <<   "C      C          S        S                !!\n";
 cout <<  "C                 S                          !!\n";
 cout << "C                   S                         !!\n";
 cout << "C                    S S S S                  !!\n";
 cout << "C                           S                 !!\n";
 cout << "C                           S                 !!\n";
 cout << "C       C        S         S\n";
 cout <<   "C C C            S S S S                    OO\n";
 cout << "***************************************************\n";      
 cout << "Computer Science is Cool Stuff!";
 
 return 0;
  }
