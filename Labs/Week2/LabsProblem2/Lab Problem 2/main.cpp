/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 12:02 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
 //Problem 2    
int a = 5;
int b = 10;
int c = a;

cout << "a: " << a << "" << "b: " << b << endl;
a = b;
b = a;
b = c;
cout << "a: " << a << "" << "b: " << b << endl;

//Problem 4 Input measurments 
double meters;

cout << "Please Enter a number in meters" << endl;
cin >> meters;

cout  << "Feet: " << (meters * 3.281) << endl;
cout  << "Inches: " << (meters * 12 * 3.281) << endl;
cout  << "Miles: " << (meters / 1609.344) << endl;
    return 0;
}

