/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Declares two variables of type integer
    int num1, num2;
    
    //output junk values
    cout << num1 << " " << num2 << endl;
    
    //Initialization
    // or num1 = 0
    // num2 = 0
    num1 = num2 = 0;
    
    //output variable again
    cout << num1 << " " << num2 << endl;
    
    //Get user input fo both values
    cout << "Please enter two integers" << endl;
    
    cin >> num1 >> num2;
    
    cout << "You Entered" << endl
         << num1 << " " << num2 << endl;
    
    //Calculate Average of two Numbers
    int total = num1 + num2;
    cout << endl << "Total: " << total << endl;
    
    //Case of integer division
    double averageIntDivision = total / 2;
    
    double averageStaticCast = static_cast<double> (total) / 2;
    double averageDecimal = total / 2.0;
    cout << "Average Int Division: " << averageIntDivision << endl; //output average
     cout << "Average Static Cast: " << averageStaticCast << endl; //output StaticCast
      cout << "Average Decimal: " << averageDecimal << endl; //output decimal
      
      //Modulus Operator
      cout << "Modulus of 7 % 3: " << 7 % 3 << endl;
    
    return 0;
}

