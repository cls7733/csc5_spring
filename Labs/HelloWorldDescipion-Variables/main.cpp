/* 
 * File:   main.cpp
 * Author: ChaMarie Spears
 * Student ID: 2427116
 * Date: 2/20/14
 * In class example Hello World 
 */ 
 

#include <cstdlib> // cstdlib libary not needed for this program
#include <iostream> // iosream libary is needed for output in programs

//Gives he context of where my libraries are coming from
using namespace std;

/*
 * 
 */

//There is always and only one main
//Programs always start at main
//Programs execute from top to bottom, left to right
int main(int argc, char** argv) {
    
    //cout specifies output
    //endl creates a new line
    //<< specifies the stream operator
    //All statements end in a ;
    
    //Using a programmer-defined identifier/variable
    //message is a variable
    //sring is a data type
    // = is an assignment operator
    //assign right to left
    string message;
    //message = "Hello World";
    
    //Prompt user
    cout << "Please Enter a message" << endl;
    
    cin >> message; //User message
    
    cout << "Your message is " << message << endl;
    
    //C++ ignores whitespace
    cout << "ChaMarie Spears" << endl;
    cout 
         << 
            "ChaMarie Spears";
    
    //if program hits return it ran successfully 
    return 0;
}//All code has to be within curly braces

